
import numpy as np
import matplotlib.pyplot as plt


# return arrayof heat(xi,ti): temperature at point xi at time ti


def Eval(alpha, L, T, n, fx):
    dx = L/n
    #print(dx)
    dt = T/n
    #print(dt)
    s = (alpha**2)*(dt/(dx**2))
    #print(s)
    H = np.zeros((n,n))
    H[:,0] = fx
    #print(H[1][0])
    for i in range(0,n):
        for j in range(1, n):
            if ((i == 0) or (i == n-1)):
                H[i][j] = 0
            else:
                H[i][j] = H[i][j-1] + s*(H[i+1][j-1] - 2*H[i][j-1] + H[i-1][j-1])
                #print(H[i][j])
                
    return H


def main():
    fx = np.zeros(10)
    for i in range(0,10):
        if (i <=5): 
            fx[i] = i*0.1
        else:
            fx[i] = 0
    Result = Eval(alpha=0.4, L=2, T=1, n=10,fx=fx)
    plt.imshow(Result, cmap='hot', interpolation='nearest')
    plt.show()


main()




